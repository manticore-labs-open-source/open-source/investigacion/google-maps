# Uso de la Api de Google Maps en Ionic

La Api de Google Maps muestra los mapas como imágenes o mapas interactivos. Añádeles los marcadores, líneas, colores, polígonos e imágenes que más le vayan a tu estilo.

Para el uso de la **Api de Google se debe tener una cuenta de Google**, una vez que se haya creado una cuenta, debemos dirigirnos a la [consola de Google](https://console.developers.google.com)

## Crear proyecto en la consola de Google
* Para el uso de la api de Google maps, se debe crear un proyecto previamente.

![imagen](imagenes/maps-new.png)

* Se debe colocar un nombre al proyecto que se va a crear.

![imagen](imagenes/maps-name.png)

* Ya creado el proyecto seleccionamos biblioteca.

![imagen](imagenes/maps-library.png)

* Nos redirigirá a las api y servicios que nos ofrece Google.

![imagen](imagenes/maps-apis.png)

* Seleccionamos la de Android

![imagen](imagenes/maps-android.png)

* habilitamos la api, nos aparecerá el siguiente mensaje:

![imagen](imagenes/maps-habilitar.png)

* Damos click en **Administrar**, nos redirigirá a la api, damos click en credenciales para poder copiar **key de la api** con la cual podremos usar la api.

![imagen](imagenes/maps-key.png)

## Uso en Ionic

* Primero se debe instalar las dependencias. Se debe cambiar **(API_KEY_FOR_ANDROID)** por la key, anteriormente copiada, la misma se puede usar para **iOS**
```
ionic cordova plugin add cordova-plugin-googlemaps \
  --variable API_KEY_FOR_ANDROID="(API_KEY_FOR_ANDROID)" \
  --variable API_KEY_FOR_IOS="(API_KEY_FOR_IOS)"
```
```
npm install --save @ionic-native/core@latest @ionic-native/google-maps@latest
```

* Se debe colocar en el app.module.ts para poder usar el módulo.

![imagen](imagenes/maps-module.png)

* Para usar y crear el mapa se usa el siguiente bloque de código: 
```javascript
// ts
const mapOptions: GoogleMapOptions = {
            controls: {
              compass: true,
              myLocation: true,
              myLocationButton: true,
              mapToolbar: true
            },
            camera: {
              target: this.ubicacionActual,
              zoom: 12
            }
          };

          this.mapaGoogleMaps = GoogleMaps.create('map_canvas', mapOptions);
```
 
* Donde el **map_canvas** se encuentra en un archivo .html, además, se le puede colocar estilos es un archivo .scss

```html
<!--html-->
<div id="map_canvas"></div>
```

```scss
//scss
#map_canvas{
        height: 50%;
        width: 100%;
    }
```




<a href="https://twitter.com/kevi_n_24" target="_blank"><img alt="Sígueme en Twitter" height="35" width="35" src="https://3.bp.blogspot.com/-E4jytrbmLbY/XCrI2Xd_hUI/AAAAAAAAIAo/qXt-bJg1UpMZmTjCJymxWEOGXWEQ2mv3ACLcBGAs/s1600/twitter.png" title="Sígueme en Twitter"/> @kevi_n_24 </a><br>



